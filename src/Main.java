import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static final String JDBC_URL = "jdbc:mysql://localhost:3306/java1pc9_13mai";
    public static final String USER = "root";
    public static final String PASSWORD = "";

    public static void main(String[] args) {
        String selectStudents = "SELECT * FROM students";
        String selectStudentByFirstName = "SELECT * FROM students WHERE firstName = ?";

        try (Connection connection = DriverManager.getConnection(JDBC_URL, USER, PASSWORD);
             Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             ResultSet resultSet = statement.executeQuery(selectStudents);
             PreparedStatement preparedStatement = connection.prepareStatement(selectStudentByFirstName)) {

            //insertStudent(statement);
            selectAllStudents(statement);
            selectSecondStudent(resultSet);
            //preparedStatementExample(preparedStatement);
            System.out.println("Added features");


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    private static void preparedStatementExample(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, "Alex");
        ResultSet resultSet2 = preparedStatement.executeQuery();
        List<Student> students = new ArrayList<>();
        while (resultSet2.next()){
            Student student = getStudent(resultSet2);
            students.add(student);
        }

        students.forEach(student -> System.out.println(student));
    }

    private static void selectSecondStudent(ResultSet resultSet) throws SQLException {
        List<Student> students = new ArrayList<>();

        resultSet.absolute(2);
        Student student = getStudent(resultSet);
        students.add(student);

        students.forEach(el -> System.out.println(el));
    }

    private static void selectAllStudents(Statement statement) throws SQLException {
        String selectStudents = "SELECT * FROM students";
        ResultSet resultSet = statement.executeQuery(selectStudents);
        List<Student> students = new ArrayList<>();
        while (resultSet.next()) {
            Student student = getStudent(resultSet);
            students.add(student);
        }

        students.forEach(student -> System.out.println(student));
    }

    private static Student getStudent(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String firstName = resultSet.getString(2);
        String lastName = resultSet.getString("lastName");
        String email = resultSet.getString("email");
        Date birthDate = resultSet.getDate(5);

        return new Student(id, firstName, lastName, email, birthDate.toLocalDate());
    }

    private static void insertStudent(Statement statement) throws SQLException {
        String insertStudent = "INSERT INTO students VALUES (null, 'Alexandru', 'Peticila', 'alexandru.peticila2@gmail.com', '1998/2/24')";
        int result = statement.executeUpdate(insertStudent);
        System.out.println(result);
    }
}
